import plotly.io as pio
import plotly.offline as offline
import plotly.graph_objs as go
import plotly.plotly as py
import plotly
import pandas as pd
from math import pi
from bokeh.io import output_file, save
from bokeh.palettes import  viridis, Category10
from bokeh.plotting import figure
from bokeh.transform import cumsum
import holoviews as hv
from holoviews import opts, Palette
from bokeh.palettes import Category20c
import numpy as np
from holoviews import dim, opts
import warnings
warnings.filterwarnings("ignore")
# import geoviews as gv
# import geoviews.feature as gf
# from geoviews import dim, opts
from progressbar import ProgressBar
pbar = ProgressBar()


df_m = pd.read_csv("data/clean_data.csv")
df = df_m.drop_duplicates(subset=['Respondent', 'DevType'], keep='first').copy()
df.sort_values(by=['Respondent'], ascending=True, inplace=True)

hv.extension('bokeh')
renderer = hv.renderer('bokeh')

############################################################

emp = df['Employment'].value_counts()

data = pd.Series(emp).reset_index(name='value').rename(columns={'index':'Employment'})
data['angle'] = data['value']/data['value'].sum() * 2*pi
data['color'] = Category10[len(emp)]

data['percentage'] = 100*(data['value']/data['value'].sum())

p = figure(plot_height=600,plot_width=900, title="Employment Status", toolbar_location=None,
           tools="hover", tooltips="@Employment: %@percentage", x_range=(-0.5, 1.0))

p.wedge(x=0, y=1, radius=0.4,
        start_angle=cumsum('angle', include_zero=True), end_angle=cumsum('angle'),
        line_color="white", fill_color='color', legend='Employment', source=data)

p.axis.axis_label=None
p.axis.visible=False
p.grid.grid_line_color = None

output_file("public/html/EmploymentPiechart.html")
save(p)
############################################################


opensource = df['OpenSource'].value_counts()
chart_colors = ['#FF33F0', '#33C1FF']
data = pd.Series(opensource).reset_index(name='value').rename(columns={'index':'OpenSource'})
data['angle'] = data['value']/data['value'].sum() * 2*pi
data['color'] = chart_colors[:len(chart_colors)]

data['percentage'] = 100*(data['value']/data['value'].sum())

p = figure(plot_height=400,plot_width=500, title="Working with Open Source tools", toolbar_location=None,
           tools="hover", tooltips="@OpenSource: %@percentage", x_range=(-0.5, 1.0))

p.wedge(x=0, y=1, radius=0.4,
        start_angle=cumsum('angle', include_zero=True), end_angle=cumsum('angle'),
        line_color="white", fill_color='color', legend='OpenSource', source=data)

p.axis.axis_label=None
p.axis.visible=False
p.grid.grid_line_color = None

output_file('public/html/OpenSourcePiechart.html')
save(p)
############################################################


ed = df['FormalEducation'].value_counts()

v = ed.values.tolist()
i = ed.index.tolist()

from bokeh.models import PrintfTickFormatter
formatter = PrintfTickFormatter(format='%f')

bars = hv.Bars((i, v), hv.Dimension('Education'), 'Count').opts(tools=['hover'],xformatter= formatter,show_legend=False,fill_color=dim('Education').str(),cmap='PiYG')
t = (bars.relabel('Formal Education').opts(invert_axes=True, width=1000) )

renderer.save(t, 'public/html/FormalEducationBar')


############################################################


lan_keys = [ "JavaScript", "Python", "HTML", "CSS", "Bash/Shell", "SQL", "TypeScript" ]
lan_val = [ ]
for key in lan_keys:
  key_count = df['LanguageWorkedWith'].str.count( key )
  lan_val.append( key_count.sum()  )

v = lan_val
i = lan_keys

formatter = PrintfTickFormatter(format='%f')

bars = hv.Bars((i, v), hv.Dimension('Programming Language'), 'Count').opts(tools=['hover'],yformatter= formatter,show_legend=False,fill_color=dim('Programming Language').str(),cmap='PiYG')
t = (bars.relabel('Programming Language').opts( width=600, fontsize={'title': 10, 'labels': 3, 'xlabel':8, 'ylabel':8}) )

renderer.save(t, 'public/html/LanguageWorkedWithBar')


#############################################################


frame_keys = [ 'Django', 'Angular', 'Hadoop' , 'Node.js' , 'React' , 'Spark' '.NET Core', 'Spring',  'TensorFlow' ]
frame_val = [ ]
for key in frame_keys:
  key_count = df['FrameworkWorkedWith'].str.count( key )
  frame_val.append( key_count.sum()  )

v = frame_val
i = frame_keys

bars = hv.Bars((i, v), hv.Dimension('Framework'), 'Count').opts(tools=['hover'],yformatter= formatter,show_legend=False,fill_color=dim('Framework').str(),cmap='PiYG')
t = (bars.relabel('Framework').opts( width=600) )

renderer.save(t, 'public/html/FrameworkWorkedWithBar')



##############################################################

type_keys = [ "Database administrator", "DevOps specialist", "Full-stack developer" , "System administrator", "Data or business analyst", "Desktop or enterprise applications developer", "Game or graphics developer", "QA or test developer", "Student" ]
type_val = [ ]
for key in type_keys:
  key_count = df['DevType'].str.count( key )
  type_val.append( key_count.sum()  )

v = type_val
i = type_keys

bars = hv.Bars((i, v), hv.Dimension('Developer Type'), 'Count').opts(tools=['hover'],xformatter= formatter,show_legend=False,fill_color=dim('Developer Type').str(),cmap='PiYG')
t = (bars.relabel('Developer Type').opts( invert_axes=True,width=800, height=400) )

renderer.save(t, 'public/html/DevTypebar')


############################not working###################################


country = df['Country'].value_counts()
l  =len(country)
data = pd.Series(country).reset_index(name='value').rename(columns={'index':'Country'})
data['angle'] = data['value']/data['value'].sum() * 2*pi


chart_colors = 10*Category20c[17]+Category20c[9]
data['color'] = chart_colors[:len(chart_colors)]

data['percentage'] = 100*(data['value']/data['value'].sum())

p = figure(plot_height=600,plot_width=800, title="Country", toolbar_location=None,
           tools="hover", tooltips="@Country: %@percentage", x_range=(-0.5, 1.0))

p.wedge(x=0, y=1, radius=0.4,
        start_angle=cumsum('angle', include_zero=True), end_angle=cumsum('angle'),
        line_color="white", fill_color='color', legend='Country', source=data)

p.axis.axis_label=None
p.axis.visible=False
p.grid.grid_line_color = None
p.legend.label_text_font_size = "11px"

output_file('public/html/CountryPiechart.html')
save(p)

############################################################


#############################################################



yc = df['YearsCoding'].values
x_min = min(yc)
x_max = max(yc)
mu = np.mean(yc) 
sigma = np.std(yc)

measured = np.random.normal(mu, sigma, len(yc))

hv.extension('bokeh')
renderer = hv.renderer('bokeh')

h = hv.Distribution(measured,kdims=['Years of Coding'])
from holoviews.operation.stats import univariate_kde

kde = univariate_kde(h, bin_range=(0, 25), bw_method='silverman').opts(fill_color='#33E0FF')
kde2 = (kde.relabel('Normal Distribution-Years of Coding ').opts(fontsize={'title': 9, 'labels': 3, 'xlabel':8, 'ylabel':8}) )

renderer.save(kde2, 'public/html/YearsCodingNormalDistribution')

########################### JobSatisfaction #####################################
job_sat = df['JobSatisfaction'].value_counts()

data = pd.Series(job_sat).reset_index(name='value').rename(columns={'index':'JobSatisfaction'})
data['angle'] = data['value']/data['value'].sum() * 2*pi
data['color'] = Category10[len(job_sat)]


data['percentage'] = 100*(data['value']/data['value'].sum())

p = figure(plot_height=600,plot_width=900, title="Job Satisfaction", toolbar_location=None,
           tools="hover", tooltips="@JobSatisfaction: %@percentage", x_range=(-0.5, 1.0))

p.wedge(x=0, y=1, radius=0.4,
        start_angle=cumsum('angle', include_zero=True), end_angle=cumsum('angle'),
        line_color="white", fill_color='color', legend='JobSatisfaction', source=data)

p.axis.axis_label=None
p.axis.visible=False
p.grid.grid_line_color = None

output_file("public/html/JobSatisfactionPiechart.html")
save(p)
###############################Avg. Salary by Country##########################
country_salary = df[['Country', 'ConvertedSalary']].dropna();
country_respondent = df[['Country', 'Respondent']].dropna();
country_salary = country_salary.groupby('Country', as_index=False).mean()

country_salary['ConvertedSalary'] = country_salary['ConvertedSalary'].apply(np.floor)

country_respondent = country_respondent.groupby('Country', as_index=False).count()
country_salary_respondent_count = country_salary.set_index('Country').join(country_respondent.set_index('Country'));
country_salary_respondent_count = country_salary_respondent_count[country_salary_respondent_count['Respondent'] > 500]
country_salary_respondent_count.sort_values(by=['ConvertedSalary'],inplace=True, ascending=False)
country_salary_respondent_count_labels = country_salary_respondent_count.index
country_salary_respondent_count_values = country_salary_respondent_count.values[:, 0]

country_satisfaction = df[['Country', 'JobSatisfaction']].dropna()
country_satisfaction = country_satisfaction[(country_satisfaction['JobSatisfaction'] == 'Extremely satisfied') | (
country_satisfaction['JobSatisfaction'] == 'Moderately satisfied') | (country_satisfaction['JobSatisfaction'] == 'Slightly satisfied')].copy()
country_satisfaction = country_satisfaction.groupby(['Country']).count()


v = country_salary_respondent_count.values[:, 0]
i = country_salary_respondent_count.index

from bokeh.models import PrintfTickFormatter
formatter = PrintfTickFormatter(format='%f')

bars = hv.Bars((i, v), hv.Dimension('Country'), 'Avg. Salary').opts(tools=['hover'],xformatter= formatter,show_legend=False,fill_color=dim('Country').str(),cmap='PiYG')#  '#E933FF'
t = (bars.relabel('Avg. Salary by Country').opts(invert_axes=True,height = 650, width=1000) )

renderer.save(t, 'public/html/AvgSalarybyCountryBar')

###################################Map#########################################
plotly.tools.set_credentials_file(
    username='rubencg', api_key='MCvRHoMBd0J7OERHj4pH')
# plotly.tools.set_credentials_file(username='ufshaik', api_key='N85boBvuBoHTaa6HrNwK')

df_country_codes = pd.read_csv(
    'https://raw.githubusercontent.com/plotly/datasets/master/2014_world_gdp_with_codes.csv')
salary_map = country_salary.set_index('Country').join(
    df_country_codes.set_index('COUNTRY'))
data = [go.Choropleth(
    locations=salary_map['CODE'],
    z=salary_map['ConvertedSalary'],
    text=salary_map.index,
)]

layout = go.Layout(
    title=go.layout.Title(
        text='Average Developer Salary by Country'
    ),
)

fig1 = go.Figure(data=data, layout=layout)
py.plot(fig1, filename='AverageDeveloperSalarybyCountry')
# offline.init_notebook_mode()
offline.plot({'data': data, 'layout': layout}, filename='public/html/AverageDeveloperSalarybyCountry.html', auto_open=False,
             output_type='file', image_width=1800, image_height=1600, )
# pio.write_image(fig1, 'public/images/AverageDeveloperSalarybyCountry.png')

country_satisfaction = country_satisfaction.reset_index()
country_satisfaction.rename(columns={'Country': 'COUNTRY'}, inplace=True)
satisfaction_map = pd.merge(
    country_satisfaction, df_country_codes, on=['COUNTRY'])
print("country_satisfaction")
print(satisfaction_map.head())

data = [go.Choropleth(
    locations=satisfaction_map['CODE'],
    z=satisfaction_map['JobSatisfaction'],
    text=satisfaction_map['COUNTRY'],
)]
layout = go.Layout(
    title=go.layout.Title(
        text='Job Satisfaction By Country'
    ),
)
fig2 = go.Figure(data=data, layout=layout)
py.plot(fig2, filename='JobSatisfactionByCountry')
# offline.init_notebook_mode()
offline.plot({'data': data, 'layout': layout}, filename='public/html/JobSatisfactionByCountry.html', auto_open=False,
             output_type='file', image_width=1800, image_height=1600, )
# # pio.write_image(fig2, 'public/images/JobSatisfactionByCountry.png')

####################################3
ycprof = df['YearsCodingProf'].values

boxwhisker = hv.BoxWhisker(ycprof, vdims=['Years of Coding Professionally']).opts(box_fill_color='#9633FF')
renderer.save(boxwhisker, 'public/html/YearsCodingProfboxplot')

#############################################

df_2 = df_m.copy()
df_2.drop_duplicates(subset=['Respondent', 'LanguagesWorked'], inplace=True)
df_2 = df_2[(df_2['LanguagesWorked'] != '') &
            (pd.notna(df_2['LanguagesWorked']))]
df_2 = df_2[['Respondent', 'LanguagesWorked']]
langs = df_2.LanguagesWorked.unique()
first = True

##############################################

''' Salary Column'''
df_1 = df_m[pd.notna(df_m['SalaryType'])].copy()
df_1 = df_1[pd.notna(df_1['Developer'])]
df_1 = df_1[['Developer', 'salary_1']]
df_1 = df_1[df_1['salary_1'] != 0]
df_1.rename(columns={'salary_1': 'Salary Range'}, inplace=True)
title = "Salary Box"
boxwhisker = hv.BoxWhisker(df_1, 'Developer', 'Salary Range', label=title)
boxwhisker.opts(show_legend=False, width=800, height=600, xrotation=90,
                box_fill_color=dim('Developer').str(), cmap='Set1')

renderer.save(boxwhisker, 'public/html/SALARYBOX')


def plot_corr(df, size=10):
    '''correlation matrix for each pair of columns in the dataframe.'''

    corr = df.corr()
    corr = corr.iloc[::-1]
    corr = corr[corr.columns[::-1]]
    corr.style.background_gradient(cmap='coolwarm')
    tups = [tuple(x) for x in corr.stack().reset_index().values.tolist()]    
    h = hv.HeatMap(tups).opts(opts.HeatMap(xaxis='top', xrotation=90,tools=['hover'], colorbar=True, width=600,height=600, toolbar='above'))

    hv.save(h, 'public/html/LanguageCorrelation.html')


for x in pbar(langs):
    tmp = df_2[df_2['LanguagesWorked'] == x].drop_duplicates(
        subset=['Respondent']).copy()
    tmp = pd.merge(tmp, df_2, on=['Respondent'])
    tmp1 = tmp.groupby(['LanguagesWorked_y'])[
        'LanguagesWorked_x'].count().reset_index(name=x)
    if first:
        tmp2 = tmp1.copy()
        first = False
    else:
        tmp2 = pd.merge(tmp1, tmp2, on=['LanguagesWorked_y'], how='outer')
tmp2.set_index('LanguagesWorked_y', inplace=True)
plot_corr(tmp2)
del df_2
