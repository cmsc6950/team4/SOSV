import os 
import sys

def checkDownloadResults(flag):
  if flag != 0:
    os.system('python download.py')
  histogam_exists = os.path.isfile('public/images/histogram.png') 
  data_exists = os.path.isfile('data/downloaded_data.csv') 
  assert ( histogam_exists and data_exists ), "Download file did not generated the histogram or the dataset"
  print("Histogram and Data File Created Successfully", histogam_exists, data_exists )

def checkCleanResults(flag):
  if flag != 0:
    os.system('python cleandata.py')
  clean_data_exists = os.path.isfile('data/clean_data.csv') 
  assert ( clean_data_exists ), "Clean file did not results"
  print("Clean Data File Created Successfully", clean_data_exists )


def checkVisualization(flag):
  if flag != 0:
    os.system('python visualization.py')
  output_files = [
      "public/matplotlib-images/EmployementType.png",
      'public/matplotlib-images/FormalEducation.png',
      'public/matplotlib-images/ProgrammingLanguage.png',
      'public/matplotlib-images/YearsWorkingProfessionally.png',
      'public/matplotlib-images/YearsCodingNon-Professionally.png',
      'public/matplotlib-images/Countries.png',
      'public/matplotlib-images/Framework.png',
      'public/matplotlib-images/DevType.png',
      'public/matplotlib-images/OpenSource.png',
      'public/matplotlib-images/JobSatisfaction.png',
      'public/matplotlib-images/AverageSalaryCountry.png',
      'public/matplotlib-images/SALARYBOX.png',
      'public/matplotlib-images/Languagescorr.png',
  ]
  for filepath in output_files:
    assert os.path.isfile(filepath), "{} - does not exist".format(filepath) 
  print("All Visualization Generated Successfully")


def checkBokehVisualization(flag):
  if flag != 0:
    os.system('python bokehVisualisation.py')
  output_files = [
    'public/html/EmploymentPiechart.html',
    'public/html/OpenSourcePiechart.html',
    'public/html/FormalEducationBar.html',
    'public/html/LanguageWorkedWithBar.html',
    'public/html/FrameworkWorkedWithBar.html',
    'public/html/DevTypebar.html',
    'public/html/CountryPiechart.html',
    'public/html/YearsCodingNormalDistribution.html',
    "public/html/JobSatisfactionPiechart.html",
    'public/html/AvgSalarybyCountryBar.html',
    'public/html/YearsCodingProfboxplot.html',
    'public/html/SALARYBOX.html',
    'public/html/LanguageCorrelation.html',
    'public/html/AverageDeveloperSalarybyCountry.html',
    'public/html/JobSatisfactionByCountry.html',
  ]
  for filepath in output_files:
    assert os.path.isfile(filepath), "{} - does not exist".format(filepath) 
  print("All Visualization Generated Successfully")


if __name__ == "__main__":
  run_file = 0
  try:
     run_file = sys.argv[1]
  except:
    pass    
  checkDownloadResults(run_file)
  checkCleanResults(run_file)
  checkVisualization(run_file)
  checkBokehVisualization(run_file)
