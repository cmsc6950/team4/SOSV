import warnings
import pandas as pd
from progressbar import ProgressBar
import math
pbar = ProgressBar()
warnings.filterwarnings("ignore")

try:
    df = pd.read_csv("data/downloaded_data.csv", dtype='unicode')

    print("Length of dataframe before cleaning: {}".format(len(df)))

    # Trimming the columns
    df = df[['Respondent','YearsCodingProf','YearsCoding','Employment','FormalEducation', 'Country', "OpenSource", 'JobSatisfaction',
    'UndergradMajor','DevType','Currency' ,'Salary' ,'ConvertedSalary' ,'CurrencySymbol' ,'SalaryType','LanguageWorkedWith','IDE','FrameworkWorkedWith']]

    df.dropna(thresh=9, inplace=True)
    # YearsCoding and YearsCodingProf
    def years(row):
            x = row[0].split(" ")
            y = row[1].split(" ")
            if len(x[0]) >= 3:
                x = x[0].split("-")
            if len(y[0]) >= 3:
                y = y[0].split("-")
            return x[0], y[0]

    # for strings fill 0 years
    df['YearsCodingProf'].fillna('0 years', inplace=True)
    df['YearsCoding'].fillna('0 years', inplace=True)
    # Splitting years into numbers
    df['YearsCoding'], df['YearsCodingProf'] = zip(*df[['YearsCodingProf','YearsCoding']].apply(years, axis=1))

    df['ConvertedSalary'].fillna(0, inplace=True)

    deleted = {}

    # Converting strings to floats
    def conv(x):
        try:
            return float(x[0])
        except:
            # Checking for cents in the salary
            if '.' == x[0][-3] or ',' == x[0][-3]:
                x[0]=x[0][:-3]
                x[0] = x[0].replace(",", "")
            # Formatting salary based on months and year
            elif x[1] == 'Monthly' or x[1] == 'Weekly' or pd.isnull(x[1]):
                if len(x[0]) > 4:
                    x[0] = x[0].replace(",", "")
                else:
                    x[0] = x[0].replace(",", ".")
            elif x[1] == 'Yearly':
                x[0] = x[0].replace(",", "")
            return float(x[0])
        
    # Removing Noise from salary
    def remove_noise(x):
        if len(str(x[0])) > 4 and (x[1] == 'Monthly' or x[1] == 'Weekly' or x[1] == 'N/A'):
            return float(0)
        elif len(str(x[0])) > 7 and x[1] == 'Yearly':
            return float(0)
        elif 'e' in str(x[0]):
            return float(0)
        else :
            if (x[2] == 76924):
                print(x[0], str(x[0]))
            return x[0]
                

    df['salary_1'] = df[['ConvertedSalary',
                                'SalaryType', 'Respondent']].apply(conv, axis=1)

    df['salary_1'] = df[['ConvertedSalary', 'SalaryType',
                                'Respondent']].apply(remove_noise, axis=1)

    df.fillna('N/A', inplace=True)

    df[['Respondent', 'YearsCodingProf', 'YearsCoding']] = df[['Respondent','YearsCodingProf', 'YearsCoding']].apply(pd.to_numeric)
    df[['Employment', 'FormalEducation', 'UndergradMajor','DevType', 'SalaryType', 'LanguageWorkedWith', 'IDE', 'FrameworkWorkedWith']] = df[['Employment', 'FormalEducation', 'UndergradMajor', 'DevType', 'SalaryType', 'LanguageWorkedWith', 'IDE', 'FrameworkWorkedWith']].astype(str)

    def onsplit(x, z):
        a = x[z].split(';')
        for y in a:
            tmp1.append(x['Respondent'])
            tmp2.append(str(y))
    
    def fill_gap(x,*z):
        if pd.isna(x[z[1]]):
            return x[z[0]]
        return x[z[1]]           


    multiple_extract = {'DevType': ['Dev','Developer'],
                        'LanguageWorkedWith': ['LWW','LanguagesWorked'], 'FrameworkWorkedWith': ['FWW','Frameworks']}
    tmp1 = []
    tmp2 = []
    sw = True

    for x in pbar(multiple_extract):    
        # Split Dev Type column to get better information
        df['Flag'] = df[x].apply(lambda x: ';' in x)
        df_1 = df[df['Flag'] == True].copy()
        df_2 = df[df['Flag'] == False].copy()
    
        df_1.apply(onsplit, args=[x], axis=1)
        data = {'Respondent': tmp1, multiple_extract[x][0]: tmp2}
        tmp = pd.DataFrame.from_dict(data)
        
        df_3 = pd.merge(df_1, tmp, on=['Respondent'], how="inner")
        df_4 = pd.concat([df_3, df_2], sort=True)
        
        df_4[multiple_extract[x][1]] = df_4.apply(fill_gap, args=[x,multiple_extract[x][0]], axis=1)
        df_4.drop(columns=[multiple_extract[x][0]], inplace=True)
        
        if sw:
            df_5 = df_4.copy()
            sw = False
        else:
            df_5 = pd.concat([df_4, df_5], sort=True)
            
        
        del df_3, df_1, df_2, tmp
        del tmp1[:], tmp2[:]

    print("Length of dataframe after cleaning: {}".format( df_5['Respondent'].nunique()))
    print("Length of duplicate dataframe after cleaning: {}".format(len(df_5)))

    # df.to_csv('data/clean_data.csv', index=False)
    df_5.to_csv('data/clean_data.csv', index=False)

except Exception as e:
    print( "Error:", str(e) )