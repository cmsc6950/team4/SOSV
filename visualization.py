import pandas as pd
import plotly
import os
import holoviews as hv
from holoviews import dim, opts
from matplotlib import pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
from progressbar import ProgressBar
import scipy.stats
import numpy as np
import plotly.plotly as py
import plotly.graph_objs as go
import plotly.offline as offline
import plotly.io as pio

import warnings
warnings.filterwarnings("ignore")

hv.extension('bokeh')
renderer = hv.renderer('bokeh')
pbar = ProgressBar()

df_m = pd.read_csv("data/clean_data.csv")
df = df_m.drop_duplicates(subset=['Respondent', 'DevType'], keep='first').copy()
df.sort_values(by=['Respondent'], ascending=True, inplace=True)

# df = df[0:100 ]
emp = df['Employment'].value_counts()
ed = df['FormalEducation'].value_counts()
lan = df['LanguageWorkedWith'].value_counts()
frame = df['FrameworkWorkedWith'].value_counts()
job_sat = df['JobSatisfaction'].value_counts()
ycprof = df['YearsCodingProf'].values
yc = df['YearsCoding'].values
opensource = df['OpenSource'].value_counts()
country = df['Country'].value_counts()

lan_keys = [ "JavaScript", "Python", "HTML", "CSS", "Python", "Bash/Shell", "SQL", "TypeScript" ]
lan_val = [ ]
for key in lan_keys:
  key_count = df['LanguageWorkedWith'].str.count( key )
  lan_val.append( key_count.sum()  )

frame_keys = [ 'Django', 'Angular', 'Node.js', 'Hadoop' , 'Node.js' , 'React' , 'Spark' '.NET Core', 'Spring',  'TensorFlow' ]
frame_val = [ ]
for key in frame_keys:
  key_count = df['FrameworkWorkedWith'].str.count( key )
  frame_val.append( key_count.sum()  )

type_keys = [ "Database administrator", "DevOps specialist", "Full-stack developer" , "System administrator", "Data or business analyst", "Desktop or enterprise applications developer", "Game or graphics developer", "QA or test developer", "Student" ]
type_val = [ ]
for key in type_keys:
  key_count = df['DevType'].str.count( key )
  type_val.append( key_count.sum()  )


country_salary = df[['Country', 'ConvertedSalary']].dropna();
country_respondent = df[['Country', 'Respondent']].dropna();
country_salary = country_salary.groupby('Country', as_index=False).mean()
country_respondent = country_respondent.groupby('Country', as_index=False).count()
country_salary_respondent_count = country_salary.set_index('Country').join(country_respondent.set_index('Country'));
country_salary_respondent_count = country_salary_respondent_count[country_salary_respondent_count['Respondent'] > 500]
country_salary_respondent_count.sort_values(by=['ConvertedSalary'],inplace=True, ascending=False)
country_salary_respondent_count_labels = country_salary_respondent_count.index
country_salary_respondent_count_values = country_salary_respondent_count.values[:, 0]


country_satisfaction = df[['Country', 'JobSatisfaction']].dropna()
country_satisfaction = country_satisfaction[ (country_satisfaction['JobSatisfaction'] == 'Extremely satisfied') | (country_satisfaction['JobSatisfaction'] == 'Moderately satisfied' ) | (country_satisfaction['JobSatisfaction'] == 'Slightly satisfied' ) ].copy()   
country_satisfaction = country_satisfaction.groupby(['Country']).count()



fig1, ax1 = plt.subplots()
ax1.pie(emp.values, labels=emp.index, autopct='%1.1f%%', shadow=True, startangle=90)
ax1.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.
ax1.set_title("Employement Type")
fig1.autofmt_xdate()
plt.tight_layout()
fig1.savefig('public/matplotlib-images/EmployementType.png')

fig2, ax2 = plt.subplots()
ax2.barh(ed.index, ed.values )
ax2.set_yticklabels( ed.index, rotation=45 )
ax2.set_title("Formal Education")
fig2.autofmt_xdate()
plt.tight_layout()
fig2.savefig('public/matplotlib-images/FormalEducation.png')


fig3, ax3 = plt.subplots()
ax3.bar(lan_keys, lan_val)
ax3.set_xticklabels( lan_keys, rotation=45 )
ax3.set_title("Programming Language")
plt.tight_layout()
fig3.savefig('public/matplotlib-images/ProgrammingLanguage.png')


fig4, ax4 = plt.subplots()
ax4.boxplot(ycprof, 0, 'rs', 0)
ax4.set_title( "Years Working Professionally" ) ;
plt.tight_layout()
fig4.savefig('public/matplotlib-images/YearsWorkingProfessionally.png')



x_min = min(yc)
x_max = max(yc)
mean = np.mean(yc) 
std = np.std(yc)
x = np.linspace(x_min, x_max, 100)
y = scipy.stats.norm.pdf(x,mean,std)
fig5, ax5 = plt.subplots()
ax5.axvline(x=mean)
ax5.set_title( "Gaussian Distribution - Years Coding Non-Professionally" ) ;
plt.plot(x,y, color='coral')
fig5.savefig('public/matplotlib-images/YearsCodingNon-Professionally.png')



fig6, ax6 = plt.subplots()
ax6.pie(country.values, labels=country.index, autopct='%1.1f%%', shadow=True, startangle=90)
ax6.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.
ax6.set_title("Countries")
fig6.autofmt_xdate()
plt.tight_layout()
fig6.savefig('public/matplotlib-images/Countries.png')


fig7, ax7 = plt.subplots()
ax7.bar(frame_keys, frame_val)
ax7.set_xticklabels( frame_keys, rotation=45 ) 
ax7.set_title("Frameworks")
plt.tight_layout()
fig7.savefig('public/matplotlib-images/Framework.png')



fig8, ax8 = plt.subplots()
ax8.barh(type_keys, type_val )
ax8.set_yticklabels( type_keys, rotation=45 )
ax8.set_title("Developer Type")
fig8.autofmt_xdate()
plt.tight_layout()
fig8.savefig('public/matplotlib-images/DevType.png')


fig9, ax9 = plt.subplots()
ax9.pie(opensource.values, labels=opensource.index, autopct='%1.1f%%', shadow=True, startangle=90)
ax9.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.
ax9.set_title("Open Source Survey")
fig9.autofmt_xdate()
plt.tight_layout()
fig9.savefig('public/matplotlib-images/OpenSource.png')


fig10, ax10 = plt.subplots()
ax10.pie(job_sat.values, labels=job_sat.index,
        autopct='%1.1f%%', shadow=True, startangle=90)
ax10.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.
fig10.autofmt_xdate()
ax10.set_title("Job Satisfaction")
plt.tight_layout()
fig10.savefig('public/matplotlib-images/JobSatisfaction.png')


#https://money.cnn.com/2015/05/14/pf/minimum-wage-countries-australia/index.html
fig11, ax11 = plt.subplots()
ax11.bar(country_salary_respondent_count_labels, country_salary_respondent_count_values)
ax11.set_xticklabels( country_salary_respondent_count_labels, rotation=90 ) 
ax11.set_title("Avg. Salary by Country")
plt.tight_layout()
fig11.savefig('public/matplotlib-images/AverageSalaryCountry.png')

''' Salary Column'''
df_1 = df_m[pd.notna(df_m['SalaryType'])].copy()
df_1 = df_1[pd.notna(df_1['Developer'])]
df_1 = df_1[['Developer', 'salary_1']]
df_1 = df_1[df_1['salary_1'] != 0]
df_1.rename(columns={'salary_1': 'Salary Range'}, inplace=True)
title = "Salary Box"
boxwhisker = hv.BoxWhisker(df_1, 'Developer', 'Salary Range', label=title)
boxwhisker.opts(show_legend=False, width=800, height=600, xrotation=90,
                box_fill_color=dim('Developer').str(), cmap='Set1')
hv.save(boxwhisker, 'public/matplotlib-images/SALARYBOX.png', backend='matplotlib')

''' Languages Column'''
df_2 = df_m.copy()
df_2.drop_duplicates(subset=['Respondent', 'LanguagesWorked'], inplace=True)
df_2 = df_2[(df_2['LanguagesWorked'] != '') &
            (pd.notna(df_2['LanguagesWorked']))]
df_2 = df_2[['Respondent', 'LanguagesWorked']]
langs = df_2.LanguagesWorked.unique()
first = True


def plot_corr(df, size=10):
    '''correlation matrix for each pair of columns in the dataframe.'''

    def colorbar(mappable):
        '''To get the color bars to have the same aspect ratio as axes'''
        ax = mappable.axes
        fig = ax.figure
        divider = make_axes_locatable(ax)
        cax = divider.append_axes("right", size="5%", pad=0.05)
        return fig.colorbar(mappable, cax=cax)

    corr = df.corr()
    corr = corr.iloc[::-1]
    corr = corr[corr.columns[::-1]]
    corr.style.background_gradient(cmap='coolwarm')
    fig, ax = plt.subplots(figsize=(size, size))
    im = ax.matshow(corr)
    plt.xticks(range(len(corr.columns)), corr.columns, rotation=90)
    plt.yticks(range(len(corr.columns)), corr.columns)
    colorbar(im)
    fig.savefig('public/matplotlib-images/Languagescorr.png')


for x in pbar(langs):
    tmp = df_2[df_2['LanguagesWorked'] == x].drop_duplicates(
        subset=['Respondent']).copy()
    tmp = pd.merge(tmp, df_2, on=['Respondent'])
    tmp1 = tmp.groupby(['LanguagesWorked_y'])[
        'LanguagesWorked_x'].count().reset_index(name=x)
    if first:
        tmp2 = tmp1.copy()
        first = False
    else:
        tmp2 = pd.merge(tmp1, tmp2, on=['LanguagesWorked_y'], how='outer')
tmp2.set_index('LanguagesWorked_y', inplace=True)
plot_corr(tmp2)
del df_2




# plt.show()
