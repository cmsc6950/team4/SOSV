# Stack Overflow Survey 2018  - (SOSV)


## Flow

```mermaid
graph LR;
    A[Makefile]-->B(download.py);
    B-->C{cleandata.py};
    C-->D[visualization.py];
    C-->E[bokehVisualization.py];
    E-->F{unitTest.py or test_unit.py};
    D-->F{unitTest.py};
    style A fill:#FF0000;
    style B fill:#0000FF;
    style C fill:#0000FF;
    style D fill:#0000FF;
    style E  fill:#0000FF;
    style F  fill:#FF0000;
```

## Introduction to data

Each year, SO asks the developer community about everything from their favorite technologies to their job preferences. 2018 marks the eighth year they’ve published their Annual Developer Survey results—with the largest number of respondents yet. Over 100,000 developers took the 30-minute survey in January 2018.

There are 98,855 responses in the data set. Approximately 20,000 responses were started but not included in the data set because respondents did not answer enough questions, or only answered questions with personally identifying information. Of the qualified responses, 67,441 completed the entire survey.

`Source`: https://www.kaggle.com/stackoverflow/stack-overflow-2018-developer-survey


## Data Insights

Raw Data: 

	Number of columns in data: 129
	Number of rows in data: 98855

Fetched:

	Number of columns in data: 20
	Number of rows in data: 98855

Refined:

	Number of columns in data: 15 (10 unique columns + 5 parsed columns from the 10)
	Number of Unique rows in data: 98855


## Goals

* Which programming languages are being paired together?

* Which Country has the highest Median Salary?

* What levels of education were the respondents from?

* How many developers contributes to open source projects ?


Please refer to the wiki for more information

`Wiki Link`: https://gitlab.com/cmsc6950/team4/SOSV/wikis/home

`Pages`: https://cmsc6950.gitlab.io/team4/SOSV


## Project Structure

```
project
│   .gitignore
│   .gitlab-ci.yml
|   README.md
|   Pipfile
|   Pipfile.lock
|   Makefile
|   Report.tex
|   Report.pdf
|   tank.txt
|   download.py
|   cleandata.py
|   visualization.py
|   bokehVisualisation.py
|   unitTest.py
│
└───public
│   │   index.html
│   │   tools.png
│   │
│   └───html
│   │    |   AverageDeveloperSalarybyCountry.html
│   │    |   AvgSalarybyCountryBar.html
│   │    |   CountryPiechart.html
│   │    |   DevTypebar.html
│   │    |   EmploymentPiechart.html
│   │    |   FormalEducationBar.html
│   │    |   FrameworkWorkedWithBar.html
│   │    |   JobSatisfactionByCountry.html
│   │    |   JobSatisfactionPiechart.html
│   │    |   LanguageCorrelation.html
│   │    |   LanguageWorkedWithBar.html
│   │    |   OpenSourcePiechart.html
│   │    |   SALARYBOX.html
│   │    |   YearsCodingNormalDistribution.html
│   │    |   YearsCodingProfboxplot.html
│   │ 
│   └───images
│   │    │   AverageDeveloperSalarybyCountry.png
│   │    │   averagesalarybycountry.png
│   │    │   countrypiechart.png
│   │    │   developertype.png
│   │    │   employmentstatus.png
│   │    │   FormalEducation.png
│   │    │   Framework.png
│   │    │   histogram.png
│   │    │   JobSatisfaction.png
│   │    │   JobSatisfactionByCountry.png
│   │    │   languagecorrelation.png
│   │    │   normaldistributionyearsofcoding.png
│   │    │   OpenSource.png
│   │    │   ProgrammingLanguage.png
│   │    │   SalaryBox.png
│   │    │   yearsofcodingprofessionally.png
│   │    
│   └───matplotlib-images
│   |    │   AverageSalaryCountry.png
│   |    │   Countries.png
│   |    │   DevType.png
│   |    │   EmployementType.png
│   |    │   FormalEducation.png
│   |    │   Framework.png
│   |    │   JobSatisfaction.png
│   |    │   Languagescorr.png
│   |    │   OpenSource.png
│   |    │   ProgrammingLanguage.png
│   |    │   SALARYBOX.png
│   |    │   YearsCodingNon-Professionally.png
│   |    │   YearsWorkingProfessionally.png
│   
└───data
│   │   ...
```

  
