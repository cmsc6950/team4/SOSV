# Fetching the data should be automated (create a function to do that).
# Use the requests python library.
# Use the argparse python library to write a command line program that takes arguments,
import kaggle
import os
import subprocess
import sys
import pandas as pd
import matplotlib.pyplot as plt
import argparse
from matplotlib import gridspec
import math

import warnings
warnings.filterwarnings("ignore")

os.environ['KAGGLE_USERNAME'] = "rubencg195"
os.environ['KAGGLE_KEY'] = "40ea868ca6602ce4137e0d62c593c5b8"

parser = argparse.ArgumentParser(description='Download Kraggle Dataset.')
parser.add_argument('-d', metavar='D', help='Dataset Name')
parser.add_argument('-f', metavar='F', help='Data Filename')
parser.add_argument('-c', metavar='C', help='Columns', type=str, nargs='+')

def DownloadDataset(dataset_path):
  kaggle.api.dataset_download_files(
    dataset_path, "data" , unzip=True)
  
if __name__ == "__main__":
  args = parser.parse_args()

  root_folder = "data"
  default_dataset  = 'stackoverflow/stack-overflow-2018-developer-survey'
  default_filename = "survey_results_public.csv"
  out_filename = "downloaded_data.csv"
  # columns = "Respondent YearsCodingProf YearsCoding Employment FormalEducation UndergradMajor DevType Salary SalaryType LanguageWorkedWith IDE FrameworkWorkedWith CompanySize UndergradMajor Hobby OpenSource Student EducationTypes TimeFullyProductive HackathonReasons LanguageDesireNextYear OperatingSystem Country JobSatisfaction".split(" ")
  columns = "Respondent YearsCodingProf YearsCoding Employment FormalEducation UndergradMajor DevType Currency Salary ConvertedSalary CurrencySymbol SalaryType LanguageWorkedWith IDE FrameworkWorkedWith CompanySize UndergradMajor Hobby OpenSource Student EducationTypes TimeFullyProductive HackathonReasons LanguageDesireNextYear OperatingSystem Country JobSatisfaction".split(" ")
  if args.d:
    default_dataset = args.d
    print("\nChanging Default Dataset: ", args.d)
  if args.f:
    default_filename = args.f
    print("\nChanging Data Filename: ", args.f)
  if args.c:
    columns = args.c
    print("\nChanging Data Filename: ", args.c)
  
  print("\n{} Downloading Dataset: {} From Kraggle to folder: {} With columns {}".format(sys.argv[0], default_dataset, root_folder, columns))
  try:
    DownloadDataset(dataset_path=default_dataset)
    if columns:
      dataset_df = pd.read_csv(
        root_folder+"/"+default_filename, 
        usecols=columns)
    else:
      dataset_df = pd.read_csv( root_folder+"/"+default_filename )
    # dataset_df.plot.hist(subplots=True, legend=True)
    dataset_df.to_csv(root_folder+"/"+out_filename)
    print(dataset_df.head())
    # print("COLUMNS: ", dataset_df.columns)
    dataset_df = dataset_df[:1500]
    print("Rendering Histogram from a sample of the dataset of size: ", len(dataset_df))
    # dataset_df.iloc[:, 0].value_counts().plot(kind='bar')
    categorical_features = dataset_df.columns
    cols = 10
    rows = int(math.ceil(len(categorical_features) / cols))
    gs = gridspec.GridSpec(rows, cols)
    fig = plt.figure(figsize=(25,10),dpi=100)
    # fig, ax = plt.subplots(1, len(categorical_features))
    for i, categorical_feature in enumerate(dataset_df[categorical_features]):
        # dataset_df[categorical_feature].value_counts().plot("bar", ax=ax[i]).set_title(categorical_feature)
        ax = fig.add_subplot(gs[i])
        ax.set_yticklabels([])
        ax.set_xticklabels([])
        ax.axes.get_yaxis().set_visible(False)
        ax.axes.get_xaxis().set_visible(False)
        dataset_df[categorical_feature].value_counts().plot("bar").set_title(categorical_feature)


    # fig.show()
    fig.tight_layout()
    plt.savefig("public/images/histogram.png", format="png") 
    
    # plt.show(block=False)
    # plt.pause(10)

  except Exception as error:
    print("\n\n"+str(error))
    
