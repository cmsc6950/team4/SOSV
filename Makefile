.PHONY: all  prepare-dev run-1 run-2 run-3 run-4 test-suite clean tank
all: prepare-dev run-1 run-2 run-3 run-4 test-suite clean tank

SHELL=/bin/bash

U="rubencg195"
K="40ea868ca6602ce4137e0d62c593c5b8"
USERNAME := $(if $(u),"$(u)",$(U))
KEY := $(if $(k),"$(k)",$(K))
user=$(shell id -u -n)


ifeq ($(OS),Windows_NT)
    #Windows stuff
    $(info Detected Windows Environment)
    ED=Win
else
    #Linux stuff
    $(info Detected Linux Environment)
    ED=Lin
endif

$(shell rm -r -f log)
$(shell mkdir log)

$(info Welcome ${user})
ifeq ($(ED), Win)
    wpath=/c/Users/${user}/.kaggle
else
    wpath=~/.kaggle
endif
ex=$(shell test -d ${wpath} && echo "exists")
ifeq (${ex}, exists)
     $(shell rm -f ${wpath}/kaggle.json)
     $(shell echo '{"username":${USERNAME},"key":${KEY}}' > ${wpath}/kaggle.json)
else
     $(shell mkdir -p ${wpath})
     $(shell echo '{"username":${USERNAME},"key":${KEY}}' > ${wpath}/kaggle.json)
endif




prepare-dev:
	$(info Installing pipenv)	
	@pip install pipenv >  log/dev.log
	$(info Setting up virtual env)
	$(info This might take a while.. Go grab a cofee.. ^^ )
	@pipenv install

run-1:
	@pipenv run python download.py > log/download.log
run-2:
	@pipenv run python cleandata.py > log/cleandata.log
run-3:
	@pipenv run python visualization.py > log/visualization.log
run-4:
	@pipenv run python bokehVisualisation.py > log/bokehVisualization.log
test-suite:
	@pipenv run python unitTest.py > log/unitTest.log
	@pipenv run pytest test_unit.py
clean:
	$(shell rm -f -r  __pycache__)
	$(shell rm -f -r .pytest_cache)
tank:
	@cat tank.txt
